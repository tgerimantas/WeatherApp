package com.example.gerimantas.geow;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gerimantas on 11/16/2014.
 */
public class Model implements Serializable {

    private String cod;
    private Double message;
    private City city;
    private Integer cnt;
    @SerializedName("list")
   private List<ListItem> list = new ArrayList<ListItem>();

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

   public List<ListItem> getList() {
        return list;
    }

    public void setList(List<ListItem> list) {
        this.list = list;
    }


    public class City implements Serializable {
        private Integer id;
        private String name;
        @SerializedName("coord")
        private Coordinates coordinates;
        private String country;
        private Integer population;
    //    private Sys sys;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Coordinates getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(Coordinates coordinates) {
            this.coordinates = coordinates;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public Integer getPopulation() {
            return population;
        }

        public void setPopulation(Integer population) {
            this.population = population;
        }

       /*public Sys getSys() {
            return sys;
        }

        public void setSys(Sys sys) {
            this.sys = sys;
        }
*/
        public class Coordinates implements Serializable {
            @SerializedName("lon")
            private Double longitude;
            @SerializedName("lat")
            private Double latitude;

            public Double getLongitude() {
                return longitude;
            }

            public void setLongitude(Double longitude) {
                this.longitude = longitude;
            }

            public Double getLatitude() {
                return latitude;
            }

            public void setLatitude(Double latitude) {
                this.latitude = latitude;
            }
        }

      /*public class Sys implements Serializable {
            private Integer population;

            public Integer getPopulation() {
                return population;
            }

            public void setPopulation(Integer population) {
                this.population = population;
            }
        }*/
    }

    public class ListItem implements Serializable {
        @SerializedName("dt")
        private Long dateTime;
        @SerializedName("main")
        private Temperature temperature;
        @SerializedName("weather")
        private List<Weather> weather = new ArrayList<Weather>();
        public Clouds clouds;
        public Wind wind;
        public Snow snow;
        public Sys sys;
        public String dt_txt;


        public Long getDateTime() {
            return dateTime;
        }

        public void setDateTime(Long dateTime) {
            this.dateTime = dateTime;
        }

        public Temperature getTemperature() {
            return temperature;
        }

        public void setTemperature(Temperature temperature) {
            this.temperature = temperature;
        }


        public List<Weather> getWeather() {
            return weather;
        }

        public void setWeather(List<Weather> weather) {
            this.weather = weather;
        }

        public Clouds getClouds() {
            return clouds;
        }

        public void setClouds(Clouds clouds) {
            this.clouds = clouds;
        }
        public Wind getWind() {
            return wind;
        }

        public void setWind(Wind wind) {
            this.wind = wind;
        }
        public Snow getSnow() {
            return snow;
        }

        public void setSys(Sys sys) {
            this.sys = sys;
        }
        public Sys getSys() {
          return sys;
        }
        public String getDt_txt() {
            return dt_txt;
        }

        public void setDt_txt(String dt_txt) {
            this.dt_txt = dt_txt;
        }


        public class Temperature implements Serializable {
            private Double temp;
            private Double temp_min;
            private Double temp_max;
            private Double pressure;
            @SerializedName("sea_level")
            private Double sea_level;
            @SerializedName("grnd_level")
            private Double grnd_level;
            private Double humidity;
            private Double temp_kf;

            public Double getTemp() {
                return temp;
            }

            public void setTemp(Double temp) {
                this.temp = temp;
            }

            public Double getMin() {
                return temp_min;
            }

            public void setMin(Double temp_min) {
                this.temp_min = temp_min;
            }

            public Double getTemp_max() {
                return temp_max;
            }

            public void setTemp_max(Double temp_max) {
                this.temp_max = temp_max;
            }

            public Double getPressure() {
                return pressure;
            }

            public void setPressure(Double pressure) {
                this.pressure = pressure;
            }

            public Double getSea_level() {
                return sea_level;
            }

            public void setSea_level(Double sea_level) {
                this.sea_level = sea_level;
            }

            public Double getGrnd_level() {
                return grnd_level;
            }

            public void setGrnd_level(Double temp_kf) {
                this.temp_kf = temp_kf;
            }

            public Double getHumidity() {
                return humidity;
            }
            public void setHumidity(Double humidity) {
                this.humidity = humidity;
            }

            public Double getTemp_kfl() {
                return temp_kf;
            }
            public void setTemp_kf(Double temp_kf) {
                this.temp_kf = temp_kf;
            }
        }

        public class Weather implements Serializable {
            private Integer id;
            private String main;
            private String description;
            private String icon;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getMain() {
                return main;
            }

            public void setMain(String main) {
                this.main = main;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }
        }

        public class Clouds implements Serializable{
            public String all;

            public String getAll() {
                return all;
            }

            public void setAll(String all) {
                this.all = all;
            }

        }

        public class Wind implements Serializable{
            public Double speed;
            public Double deg;

            public Double getSpeed() {
                return speed;
            }
            public void setSpeed(Double speed) {
                this.speed = speed;
            }

            public Double getDeg() {
                return deg;
            }
            public void setDeg(Double deg) {
                this.deg = deg;
            }


        }

        public class Snow implements Serializable{
            @SerializedName("3h")
            public String sn;

            public String getSn() {
                return sn;
            }
            public void setSn(String sn) {
                this.sn = sn;
            }


        }

        public class Sys implements Serializable{
               public String pod;

            public String getPod() {
                return pod;
            }
            public void setPod(String pod) {
                this.pod = pod;
            }



        }
    }

}