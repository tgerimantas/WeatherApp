package com.example.gerimantas.geow;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Gerimantas on 11/7/2014.
 */
public class JsonWeather {

    String country;
    String sunrise;
    String sunset;
    String name;
    String icon;
    String main;
    String description;
    Double temp;
    byte []iconByte;


    public void getAll(String data) throws JSONException {

        JSONObject r = new JSONObject(data);

        setName(r.getString("name"));

          JSONObject sys = r.getJSONObject("sys");
          setCountry(sys.getString("country"));
          //setSunrise(sys.getString("sunrise"));
          setSunset(sys.getString("sunset"));

        JSONArray jArr = r.getJSONArray("weather");

        JSONObject JSONWeather = jArr.getJSONObject(0);
        setDescription(JSONWeather.getString("description"));
        setMain(JSONWeather.getString("main"));
        setIcon(JSONWeather.getString("icon"));

       JSONObject main = r.getJSONObject("main");
        setTemp(main.getDouble("temp"));
 //  */


    }

    public String getPlace(){
        String string = null;
        string = country+","+name;

        return string;
    }

    private void setTemp(double temp) {
        this.temp = temp;
    }
    private void setIcon(String icon) {
        this.icon = icon;
        Log.d("Myapp", icon);    }
    private void setMain(String main) {
        this.main = main;
    }
    private void setDescription(String description) {
        this.description = description;
    }

    public Double getTemp(){
        return temp;
    }
    public String getIcon(){
        return icon;
    }
    public String getMain(){
        return main;
    }
    public String getDescription(){
        return description;
    }


    private  void setName(String name){
        this.name = name;
    }
    private void setCountry(String country) {
        this.country = country;
    }
    private void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }
    private void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getCountry(){
        return country;
    }
    public String getName(){
        return name;
    }
    public String getSunrise(){
        return sunrise;
    }
    public String getSunset(){
        return sunset;
    }

    public void setIconByte(byte[] iconByte) {
        this.iconByte = iconByte;
    }
    public byte[] getIconByte(){
return iconByte;
    }

}
