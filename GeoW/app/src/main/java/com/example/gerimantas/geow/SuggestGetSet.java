package com.example.gerimantas.geow;

/**
 * Created by Gerimantas on 11/7/2014.
 */
public class SuggestGetSet {

    String country,name;
    public SuggestGetSet(String name, String country){
        this.setCountry(country);
        this.setName(name);
    }
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}