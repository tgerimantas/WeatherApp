package com.example.gerimantas.geow;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gerimantas on 11/16/2014.
 */
public class CurrentWeather {

    private Coord coord;
    private Sys sys;
    @SerializedName("weather")
    private List<Weather> weather = new ArrayList<Weather>();
    private String base;
    private Main main;
    private Wind wind;
    private Clouds Clouds;
    private  Long dt;
    private  String id;
    private  String name;
    private  String cod;

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public Sys getSys() {
            return sys;
        }

    public void setSys(Sys sys) {
            this.sys = sys;
        }


    public List<Weather> getList() {
        return weather;
    }

    public void setList(List<Weather> weather) {
        this.weather = weather;
    }


    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }
    public  Wind  getWind() {
        return wind;
    }

    public void setWind( Wind wind) {
        this.wind = wind;
    }
    public Clouds getCurrentClouds() {
        return Clouds;
    }

    public void Clouds(Clouds Clouds) {
        this.Clouds = Clouds;
    }

    public Long getDt() {
        return dt;
    }

    public void setDt(Long dt) {
        this.dt = dt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }


    public class Coord implements Serializable{
        public Double lon;
        public Double lat;

        public Double getLon() {
            return lon;
        }
        public void setLon(Double lon) {
            this.lon = lon;
        }

        public Double getLat() {
            return lat;
        }
        public void setLat(Double lat) {
            this.lat = lat;
        }

    }

    public class Sys implements Serializable{
        private int type;
        private int id;
        private String message;
        private String country;
        private String sunrise;
        private String sunset;

        public int getType() {
            return type;
        }
        public void setType(int type) {
            this.type = type;
        }

        public int getId() {
            return id;
        }
        public void setId(int id) {
            this.id = id;
        }
        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getCountry() {
            return country;
        }
        public void setCountry(String country) {
            this.country = country;
        }

        public String getSunrise() {
            return sunrise;
        }
        public void setSunrise(String sunrise) {
            this.sunrise = sunrise;
        }

        public String getSunset() {
            return sunset;
        }
        public void setSunset(String sunset) {
            this.sunset = sunset;
        }
    }

    public class Weather implements Serializable {
        private Integer id;
        private String main;
        private String description;
        private String icon;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }

    public class Main implements Serializable {
        private Double temp;
        private Double pressure;
        private Double humidity;
        private Double temp_min;
        private Double temp_max;

        public Double getTemp() {
            return temp;
        }

        public void setTemp(Double temp) {
            this.temp = temp;
        }

        public Double getPressure() {
            return pressure;
        }

        public void setPressure(Double pressure) {
            this.pressure = pressure;
        }

        public Double getHumidity() {
            return humidity;
        }
        public void setHumidity(Double humidity) {
            this.humidity = humidity;
        }

        public Double getMin() {
            return temp_min;
        }

        public void setMin(Double temp_min) {
            this.temp_min = temp_min;
        }

        public Double getTemp_max() {
            return temp_max;
        }

        public void setTemp_max(Double temp_max) {
            this.temp_max = temp_max;
        }

    }

    public class Wind implements Serializable{
        public Double speed;
        public Double deg;

        public Double getSpeed() {
            return speed;
        }
        public void setSpeed(Double speed) {
            this.speed = speed;
        }

        public Double getDeg() {
            return deg;
        }
        public void setDeg(Double deg) {
            this.deg = deg;
        }

    }

    public class Clouds implements Serializable{
        public String all;

        public String getAll() {
            return all;
        }

        public void setAll(String all) {
            this.all = all;
        }

    }

}
