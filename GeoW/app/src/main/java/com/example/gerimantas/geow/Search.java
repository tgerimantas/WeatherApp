package com.example.gerimantas.geow;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Toast;
import android.view.LayoutInflater;
import android.support.v7.app.ActionBar;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.AdapterView.OnItemClickListener;



public class Search extends ActionBarActivity implements OnItemClickListener {

     String getPlaceItem = "";
    RadioButton RadioButtonC;
    RadioButton  RadioButtonF;
    boolean RadioButtonCa;
    boolean RadioButtonFa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        RadioButtonC  = (RadioButton)findViewById(R.id.RadioButtonC);
        RadioButtonF  = (RadioButton)findViewById(R.id.RadioButtonF);


        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.search_actionbar, null);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        AutoCompleteTextView acTextView = (AutoCompleteTextView) findViewById(R.id.search_place_box);
        acTextView.setAdapter(new SuggestionAdapter(this,acTextView.getText().toString()));
        acTextView.setOnItemClickListener(this);


        SharedPreferences sharedPreferences = getSharedPreferences("default", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

         RadioButtonCa = sharedPreferences.getBoolean("RadioButtonC", false);
         RadioButtonFa = sharedPreferences.getBoolean("RadioButtonF", false);

        if((RadioButtonCa == false)&&(RadioButtonFa == false)){

            editor.putBoolean("RadioButtonC",true);             //Kai default
            editor.putBoolean("RadioButtonF", false);
            editor.putString("TemperatureMetric", "C");
            RadioButtonC.setChecked(true);

            editor.commit();
        }else if(RadioButtonCa == true){
            RadioButtonC.setChecked(true);                     //Kai tam tikra salyga

        }else if(RadioButtonFa == true) {

            RadioButtonF.setChecked(true);
        }



    }

    public void cancelAddWeather(View view) {
        Toast.makeText(Search.this, "Cancel Clicked", Toast.LENGTH_SHORT).show();
        super.onResume();
        finish();
    }

    public void addWeather(View view) {

        SharedPreferences preferences = getSharedPreferences("default",MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();


            Log.d("Myapp",getPlaceItem + "KRC" );
            editor.putString("CountryName", getPlaceItem);
            editor.commit();

            super.onResume();
            finish();


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        getPlaceItem =(String) adapterView.getItemAtPosition(position);
        Toast.makeText(Search.this, getPlaceItem, Toast.LENGTH_SHORT).show();

    }

    public void RadioButton(View view) {

        SharedPreferences sharedPreferences = getSharedPreferences("default", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        RadioButtonCa = sharedPreferences.getBoolean("RadioButtonC", false);
        RadioButtonFa = sharedPreferences.getBoolean("RadioButtonF", false);


        if(RadioButtonC.isChecked() == true){
            editor.putBoolean("RadioButtonC", RadioButtonC.isChecked());
            editor.putBoolean("RadioButtonF", RadioButtonF.isChecked());
            editor.putString("TemperatureRate", "&units=metric");
            editor.putString("TemperatureMetric", "C");
            RadioButtonC.setChecked(true);
            RadioButtonF.setChecked(false);

            editor.commit();

        }else if( RadioButtonF.isChecked() == true ){
            editor.putBoolean("RadioButtonC", RadioButtonC.isChecked());
            editor.putBoolean("RadioButtonF", RadioButtonF.isChecked());
            editor.putString("TemperatureRate", "&units=imperial");
            editor.putString("TemperatureMetric", "F");
            RadioButtonF.setChecked(true);
            RadioButtonC.setChecked(false);

            editor.commit();

        }
    }

}
