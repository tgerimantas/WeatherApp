package com.example.gerimantas.geow;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.Calendar;
import java.util.List;

public class FragmentTodayWeather extends Fragment {

    private TextView date;
    private TextView temperature;
    private TextView description;
    private TextView country;
    private TextView name;
    private ImageView icon;
    private TextView temperature_metric;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_taday_weather, container, false);

        // place = (TextView) findViewById(R.id.place);
        date = (TextView) view.findViewById(R.id.date);
        //state = (TextView) findViewById(R.id.state);
        temperature = (TextView) view.findViewById(R.id.temperature);
        // sunrise = (TextView) findViewById(R.id.sunrise);
        //sunset = (TextView) findViewById(R.id.sunset);
        description = (TextView) view.findViewById(R.id.description);
        country = (TextView) view.findViewById(R.id.country);
        name = (TextView) view.findViewById(R.id.name);
         icon = (ImageView) view.findViewById(R.id.icon);
        temperature_metric = (TextView) view.findViewById(R.id.temperature_metric);
        //TextView mainDate = (TextView) view.findViewById(R.id.mainDate);



        String  infoWeather = this.getArguments().getString("InfoWeather");
        String infoTempRate = this.getArguments().getString("TemperatureRate");
        CurrentWeather response = new Gson().fromJson(infoWeather, CurrentWeather.class);


        name.setText(response.getName());
        //country.setText(response.getSys().getCountry().toString());
        //temperature.setText(response.getMain().getTemp().toString());

        List<CurrentWeather.Weather> orai = response.getList();
        Log.d("AAA", String.valueOf(orai.size()));
        description.setText(orai.get(0).getDescription());

       if(infoTempRate.equals("&units=imperial") ){
            temperature_metric.setText("F");
            Double temp = response.getMain().getTemp();
            int temps = (int) (((temp *9)/5)-459.67);
            temperature.setText(String.valueOf(temps));
        }else{
            temperature_metric.setText("C");
            Double temp = response.getMain().getTemp();
            int temps = (int) (temp -273.15);
            temperature.setText(String.valueOf(temps));
        }

        String image = response.getList().get(0).getIcon();
        image = "@drawable/"+"_"+image;
        Log.d("AAA", image);
        int imageResource = getResources().getIdentifier(image, null, BuildConfig.class.getPackage().getName());
        Drawable res = this.getResources().getDrawable(imageResource);
        icon.setImageDrawable(res);



        return view;
	}
}
