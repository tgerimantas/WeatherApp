package com.example.gerimantas.geow;

/**
 * Created by Gerimantas on 11/7/2014.
 */
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonParse {
    double current_latitude,current_longitude;
    public JsonParse(){}
    public JsonParse(double current_latitude,double current_longitude){
        this.current_latitude=current_latitude;
        this.current_longitude=current_longitude;
    }
    public List<SuggestGetSet> getParseJsonWCF(String place)
    {
        List<SuggestGetSet> ListData = new ArrayList<SuggestGetSet>();
        try {

           // URL js = new URL("http://api.openweathermap.org/data/2.5/find?q="+place+"&type=like");
           /* URLConnection jc = null;
            while(jc == null) {
                Log.d("Connection", "Nope");
                jc = js.openConnection();
            }
            jc.addRequestProperty("x-api-key","09331402361cce576e8ebd59a3a40544");
            */
          //  BufferedReader reader = new BufferedReader(new InputStreamReader(jc.getInputStream()));



            URL url = new URL(("http://api.openweathermap.org/data/2.5/find?q="+place+"&type=like"));
            HttpURLConnection connection = null;
            while(connection == null) {
                connection =(HttpURLConnection)url.openConnection();
                connection.addRequestProperty("x-api-key","09331402361cce576e8ebd59a3a40544");
                Log.d("Connection", "Nope");
                connection.connect();
            }
            Log.d("MyApp","Connected to the server");
            InputStream is = connection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = br.readLine();
            JSONObject jsonResponse = new JSONObject(line);
            JSONArray jsonArray = jsonResponse.getJSONArray("list");

            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject r = jsonArray.getJSONObject(i);

                String name = r.getString("name");

                Log.d("Myapp","Name "+name);

                JSONObject sys = r.getJSONObject("sys");
                String country = sys.getString("country");
                Log.d("Myapp","country "+country);

               ListData.add(new SuggestGetSet(name, country));
            }
            is.close();
            connection.disconnect();
            Log.d("MyApp", "Close connection");


        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return ListData;

    }

}