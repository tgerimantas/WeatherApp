package com.example.gerimantas.geow;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import static android.widget.AdapterView.*;


public class MainWeather extends ActionBarActivity{

    ListView mainListView;
    ArrayList<String> weatherList;
    ArrayList<String> forecastList;
    boolean resumeHasRun = false;
    CustomApdapter custom;
    WeatherTask task;
    String TemperatureRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_weather);
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.main_weather_action_bar, null);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);



        final SharedPreferences sharedPreferences = getSharedPreferences("default", MODE_PRIVATE);
        Integer listSize = sharedPreferences.getInt("List_Size", 0);
        TemperatureRate = sharedPreferences.getString("TemperatureRate", "");
        final SharedPreferences.Editor editor = sharedPreferences.edit();
       // editor.putString("TemperatureRate", "&units=metric");

/*
   editor.remove("RadioButtonC");
  editor.remove("RadioButtonF");
   editor.remove("List_Size");
     editor.remove("WeatherList");
        editor.remove("ForecastList");
  editor.remove("CountryName");
*/
        editor.commit();

        mainListView = (ListView) findViewById(R.id.list);
        mainListView.setClickable(true);

        weatherList = new ArrayList<String>();
        forecastList = new ArrayList<String>();

        Log.d("Myapp", "listSize"+ listSize.toString());

        for (int i = 0; i < listSize; i++) {

            weatherList.add(sharedPreferences.getString("WeatherList" + " " + i, null));
            forecastList.add(sharedPreferences.getString("ForecastList"+ " "+ i, null));
           // Log.d("Myapp", "Prideda i lista");
        }


        custom = new CustomApdapter(weatherList, this, TemperatureRate );

        mainListView.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3)
            {

               String selected= (String) adapter.getItemAtPosition(position);
               String forecast = forecastList.get(position);
               editor.putString("SelectedWeather", selected);
               editor.putString("SelectedForecast", forecast);
               editor.commit();
                //Toast.makeText(MainWeather.this, selected., Toast.LENGTH_SHORT).show();
               Intent intent = new Intent(MainWeather.this, MainActivity.class);
               startActivity(intent);
            }
        });

        final Dialog dialog = new Dialog(MainWeather.this);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle("Delete...");

        mainListView.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView<?> adapter, View v, final int position, long arg3) {

                dialog.show();

                Button dialogButtonYes = (Button) dialog.findViewById(R.id.dialogYesButton);
                dialogButtonYes.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        weatherList.remove(position);
                        forecastList.remove(position);
                        dialog.dismiss();

                        int listSize = sharedPreferences.getInt("List_Size",0);
                        listSize = listSize  -1;
                        editor.putInt("List_Size",listSize);

                        editor.remove("WeatherList");
                        editor.remove("ForecastList");
                        editor.commit();
                        for (int i = 0; i < weatherList.size(); i++) {

                            editor.putString("WeatherList" + " " + i, weatherList.get(i));
                            editor.putString("ForecastList" + " " + i, forecastList.get(i));
                            Log.d("Myapp", "Prideda i paprasta lista");
                            editor.commit();
                        }
                        custom.notifyDataSetChanged();

                    }
                });

                Button dialogButtonNo = (Button) dialog.findViewById(R.id.dialogNoButton);
                dialogButtonNo.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();
                    }
                });


                return false;
            }
        });


        mainListView.setAdapter(custom);
    }

    @Override
    public void onResume(){
        super.onResume();

        if(!resumeHasRun) {
            resumeHasRun = true;
            Log.d("Myapp", "First time opened");
            return;
        }
        if (weatherList != null) {
            custom.notifyDataSetChanged();

            SharedPreferences sharedPreferences = getSharedPreferences("default", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            String newPlace = sharedPreferences.getString("CountryName", "");
            TemperatureRate = sharedPreferences.getString("TemperatureRate", "");
            if(newPlace != "") {

               editor.remove("CountryName");
               editor.commit();

               task = new WeatherTask();
               task.execute(newPlace);

                mainListView.setAdapter(custom);
            }
        } else {
            Toast.makeText(MainWeather.this, "Something wrong", Toast.LENGTH_LONG).show();

        }

    }



    public void onBackPressed(){
        super.onBackPressed();
        resumeHasRun = false;
        finish();
    }

    public void RefreshButton(View view) {
        //Toast.makeText(MainWeather.this, "Refresh Clicked", Toast.LENGTH_SHORT).show();

        Refresh task = new Refresh();
        task.execute();
    }

    public void CreateWeather(View view) {
        Toast.makeText(MainWeather.this, "Create Clicked", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(MainWeather.this, Search.class);
       startActivity(intent);

    }



    private class Refresh extends AsyncTask<String, String, String> {

        ArrayList<String> weather;
        ArrayList<String> forecast;

        @Override
        protected String doInBackground(String... strings) {

            String WeatherApi ="http://api.openweathermap.org/data/2.5/weather?q=";
            String ForecastApi = "http://api.openweathermap.org/data/2.5/forecast?q=";

            weather = new ArrayList<String>();
            forecast = new ArrayList<String>();

            for (int i = 0; i < forecastList.size(); i++) {

                Model response = new Gson().fromJson(forecastList.get(i), Model.class);
                String country = response.getCity().getName(); //City
                String city = response.getCity().getCountry(); //Country
                Log.d("abc", country+ "" + city);
                FetchWeatherData data = new FetchWeatherData();
                String CurrentWeatherInfo = data.getWeatherData(WeatherApi + country+","+city);
                String  Forecast = data.getWeatherData(ForecastApi +  country+","+city);

                Log.d("abc", CurrentWeatherInfo);

                weather.add(CurrentWeatherInfo);
                forecast.add(Forecast);

            }

            return  null;
        }

        protected void onPostExecute(String text) {

            SharedPreferences sharedPreferences1 = getSharedPreferences("default", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences1.edit();

            editor.remove("WeatherList");
            editor.remove("ForecastList");
            weatherList.clear();
            forecastList.clear();
            editor.commit();


            for (int i = 0; i < weather.size(); i++) {

                editor.putString("WeatherList" + " " + i, weather.get(i));
                editor.putString("ForecastList"+ " "+ i, forecast.get(i));
                weatherList.add(weather.get(i));
                forecastList.add(forecast.get(i));

            }
            Log.d("AAAA", String.valueOf(weatherList.size()));
            editor.commit();
            Toast.makeText(MainWeather.this, "Refreshed", Toast.LENGTH_SHORT).show();
            custom.notifyDataSetChanged();
        }


    }


        private class WeatherTask extends AsyncTask<String, String, String> {

        String CurrentWeatherInfo;
        String Forecast;

        @Override
        protected String doInBackground(String... strings) {
            String WeatherApi ="http://api.openweathermap.org/data/2.5/weather?q=";
            String ForecastApi = "http://api.openweathermap.org/data/2.5/forecast?q=";


            Log.d("MyApp", "Fetch weather info");
            FetchWeatherData data = new FetchWeatherData();
            CurrentWeatherInfo = data.getWeatherData(WeatherApi+strings[0]);
            Forecast = data.getWeatherData(ForecastApi+strings[0]);


            Log.d("Weather",  CurrentWeatherInfo);
            Log.d("Weather",  Forecast);


            //CurrentWeather response = new Gson().fromJson(JSON, CurrentWeather.class);


            /*List<Model.ListItem> orai = response.getList();
            List<Model.ListItem.Weather> weathers= orai.get(0).getWeather();
            String weather = weathers.get(0).getMain();
            Log.d("Ats", weather.toString());

*/
            return CurrentWeatherInfo ;
        }

        @Override
        protected void onPostExecute(String text) {

            SharedPreferences sharedPreferences1 = getSharedPreferences("default", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences1.edit();

            editor.remove("WeatherList");
            editor.remove("ForecastList");
            editor.commit();

            weatherList.add(CurrentWeatherInfo);
            forecastList.add(Forecast);

            editor.putInt("List_Size", weatherList.size());
            Log.d("Myapp", "List Size after put item"+Integer.toString(weatherList.size()));
            for (int i = 0; i < weatherList.size(); i++) {

                editor.putString("WeatherList" + " " + i, weatherList.get(i));
                editor.putString("ForecastList"+ " "+ i, forecastList.get(i));
                Log.d("Myapp", "Prideda i paprasta lista");
            }

            editor.commit();

            custom.notifyDataSetChanged();
        }
    }


}