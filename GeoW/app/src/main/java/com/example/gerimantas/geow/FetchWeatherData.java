package com.example.gerimantas.geow;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
/**
 * Created by Gerimantas on 11/7/2014.
 */
public class FetchWeatherData {

    private String WeatherApi ="http://api.openweathermap.org/data/2.5/weather?q=";
    private String Forecast = "http://api.openweathermap.org/data/2.5/forecast?q=";
    private String WeatherImageApi="http://openweathermap.org/img/w/";


    public String getWeatherData(String location){
        StringBuffer buffer = null;


        Log.d("MyApp","->GetWeatherData");
        try {

            URL url = new URL((location));
            HttpURLConnection connection = null;
            while(connection == null) {
                connection =(HttpURLConnection)url.openConnection();
                connection.addRequestProperty("x-api-key","09331402361cce576e8ebd59a3a40544");
                Log.d("Connection", "Nope");
                connection.connect();
            }

            Log.d("MyApp","Connected to the server");

            buffer = new StringBuffer();
            InputStream is = connection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while (  (line = br.readLine()) != null ) {
                buffer.append(line + "\r\n");
                Log.d("MyApp",line);
            }


            is.close();
            connection.disconnect();
            Log.d("MyApp", "Close connection");

        } catch (Exception  e) {
            e.printStackTrace();
            return null;
        }
        return buffer.toString();
    }

    public byte[] Images(String code){
        ByteArrayOutputStream baos = null;

        try {
            URL url = new URL(WeatherImageApi+code+".png");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.addRequestProperty("x-api-key", "09331402361cce576e8ebd59a3a40544");

            connection.connect();
            Log.d("Myapp", "Images -> connected");

            byte[] image_buffer = new byte[1024];      //allocate image buffer
            InputStream is = connection.getInputStream(); //input stream
            baos = new ByteArrayOutputStream();

            while(is.read(image_buffer) != -1){
                baos.write(image_buffer);
            }

            is.close();
            connection.disconnect();
            Log.d("Myapp", "Image -> Disconnect from server");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }

}
