package com.example.gerimantas.geow;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class FragmentForecast extends  ListFragment {
    ListView list;
    ArrayList<Model.ListItem> weatherList;
    boolean resumeHasRun = false;
    CustomAdapterForecast forecast;


    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_forecast, container, false);
        list = (ListView) view.findViewById(android.R.id.list);

        String weathers = this.getArguments().getString("Forecast");
        String infoTempRate = this.getArguments().getString("TemperatureRate");

        Model fore = new Gson().fromJson(weathers,Model.class);

        List<Model.ListItem> orai = fore.getList();
        Log.d("AAA", String.valueOf(orai.size()));

        weatherList = new ArrayList<Model.ListItem>();
        for(Model.ListItem l : orai){

            weatherList.add(l);
        }

        forecast = new CustomAdapterForecast(getActivity(), weatherList, infoTempRate);
        list.setAdapter(forecast);
		
		return view;
	}

}
