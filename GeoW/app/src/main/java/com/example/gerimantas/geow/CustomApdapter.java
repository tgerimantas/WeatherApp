package com.example.gerimantas.geow;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by Gerimantas on 11/13/2014.
 */
public class CustomApdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list = new ArrayList<String>();
    private Context context;
    private  String infoTempRate;



    public CustomApdapter(ArrayList<String> list, Context context, String infoTempRate) {
        this.list = list;
        this.context = context;
        this.infoTempRate = infoTempRate;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_list_view, null);
        }

       TextView listItemText = (TextView)view.findViewById(R.id.list_string_item);
       TextView temperature = (TextView) view.findViewById(R.id.temperature);
       TextView temperature_metric = (TextView) view.findViewById(R.id.temperature_metric);
       ImageView mainIcon = (ImageView) view.findViewById(R.id.mainIcon);
       TextView mainDescription = (TextView) view.findViewById(R.id.mainDescription);

       String info = list.get(position).toString();
       CurrentWeather response = new Gson().fromJson(info, CurrentWeather.class);

       listItemText.setText(response.getName());

        if(infoTempRate.equals("&units=imperial") ){
            temperature_metric.setText("F");
            Double temp = response.getMain().getTemp();
            int temps = (int) (((temp *9)/5)-459.67);
            temperature.setText(String.valueOf(temps));
        }else{
            temperature_metric.setText("C");
            Double temp = response.getMain().getTemp();
            int temps = (int) (temp -273.15);
            temperature.setText(String.valueOf(temps));
        }


        String image = response.getList().get(0).getIcon();
        image = "@drawable/"+"_"+image;
        int imageResource = context.getResources().getIdentifier(image, null, context.getPackageName());
        Drawable res = context.getResources().getDrawable(imageResource);
        mainIcon.setImageDrawable(res);

        mainDescription.setText(response.getList().get(0).getDescription());


        return view;
    }


}