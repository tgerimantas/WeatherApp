package com.example.gerimantas.geow;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;


public class SelectedWeather extends ActionBarActivity {

    private TextView place;
    private TextView date;
    private TextView state;
    private TextView temperature;
    private TextView sunrise;
    private TextView country;
    private TextView name;
    private TextView sunset;
    private ImageView icon;
    private TextView description;
    private TextView temperature_metric;
    private String SelectedCityWeatherInfo;
    private String TemperatureRate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleceted_weather);

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.selected_weather_actionbar, null);
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

       // place = (TextView) findViewById(R.id.place);
       date = (TextView) findViewById(R.id.date);
       //state = (TextView) findViewById(R.id.state);
       temperature = (TextView) findViewById(R.id.temperature);
       // sunrise = (TextView) findViewById(R.id.sunrise);
        //sunset = (TextView) findViewById(R.id.sunset);
        description = (TextView) findViewById(R.id.description);
        country = (TextView) findViewById(R.id.country);
        name = (TextView) findViewById(R.id.name);
        icon = (ImageView) findViewById(R.id.icon);
        temperature_metric = (TextView) findViewById(R.id.temperature_metric);



        //Log.d("Myapp", "place "+newPlace);

        WeatherTask task = new WeatherTask();
        task.execute(SelectedCityWeatherInfo);

    }


    public void BackToMain(View view) {
        Toast.makeText(SelectedWeather.this, "Back button Clicked", Toast.LENGTH_SHORT).show();
        super.onResume();
        finish();
    }

    public void Refresh(View view) {
        Toast.makeText(SelectedWeather.this, "Refresh Clicked", Toast.LENGTH_SHORT).show();
        WeatherTask task = new WeatherTask();
       // task.execute(newPlace+TemperatureRate);
    }

    private class WeatherTask extends AsyncTask<String, String, JsonWeather> {

        @Override
        protected JsonWeather doInBackground(String... strings) {

            JsonWeather jsonWeather = new JsonWeather();

            Log.d("MyApp", "Fetch weather info");
            FetchWeatherData datas = new FetchWeatherData();
            String data = datas.getWeatherData(strings[0]);

            try {
                jsonWeather.getAll(data);

                String icon = jsonWeather.getIcon();

                Log.d("Myapp", icon);
                byte[] iconByte = datas.Images(icon);
                jsonWeather.setIconByte(iconByte);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("Myapp", "Fetch weather image");

            return jsonWeather;
        }

        protected void onPostExecute(JsonWeather jsonWeather) {


            if (jsonWeather.iconByte != null && jsonWeather.iconByte.length > 0) {
                Bitmap img = BitmapFactory.decodeByteArray(jsonWeather.iconByte, 0, jsonWeather.iconByte.length);
                icon.setImageBitmap(img);
            }

             country.setText(jsonWeather.getCountry());
             name.setText(jsonWeather.getName());
          ///  sunset.setText(weather.getSunset());
          // place.setText(weather.getPlace());
           // sunrise.setText(weather.getSunrise());
           // state.setText(weather.getDescription());
           // data = datas.getWeatherData(strings[0]);
            SharedPreferences sharedPreferences = getSharedPreferences("default", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            //editor.putString("TemperatureRate", "&units=imperial");
            String rate = sharedPreferences.getString("TemperatureRate", "");
            if(rate.equals("&units=imperial") ){
                temperature_metric.setText("F");
            }else{
                temperature_metric.setText("C");
            }

            description.setText(jsonWeather.getDescription().toString());
           temperature.setText(jsonWeather.getTemp().toString());


        }


    }

}


