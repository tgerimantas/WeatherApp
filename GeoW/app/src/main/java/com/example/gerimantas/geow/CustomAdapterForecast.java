package com.example.gerimantas.geow;

import android.content.Context;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.HOUR_OF_DAY;

/**
 * Created by Gerimantas on 11/18/2014.
 */
public class CustomAdapterForecast extends BaseAdapter {
    ArrayList<Model.ListItem> forecastlist = new ArrayList<Model.ListItem>();
    Context context;
    String infoTempRate;

    public CustomAdapterForecast(Context context , ArrayList<Model.ListItem> forecastList, String  infoTempRate) {
        this.context = context;
        this.forecastlist = forecastList;
        this.infoTempRate = infoTempRate;

    }

    @Override
    public int getCount() {
        return forecastlist.size();
    }

    @Override
    public Object getItem(int pos) {
        return forecastlist.get(pos);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View fview = view;
        Long date;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            fview = inflater.inflate(R.layout.customforecastlist, null);
        }

       TextView temperature = (TextView)fview.findViewById(R.id.forecastTemp);
       TextView temperature_metric = (TextView)fview.findViewById(R.id.forecastMetric);
       TextView forecastDescription = (TextView)fview.findViewById(R.id.forecastDescription);
       TextView forecastDateDay = (TextView)fview.findViewById(R.id.forecastDateDay);
       TextView forecastDateHour = (TextView)fview.findViewById(R.id.forecastDateHour);
       ImageView forecastIcon = (ImageView) fview.findViewById(R.id.forecastIcon);



        forecastDescription.setText(forecastlist.get(i).getWeather().get(0).getDescription());

        if(infoTempRate.equals("&units=imperial") ){
            temperature_metric.setText("F");
            Double temp = forecastlist.get(i).getTemperature().getTemp();
            int temps = (int) (((temp *9)/5)-459.67);
            temperature.setText(String.valueOf(temps));
        }else{
            temperature_metric.setText("C");
            Double temp = forecastlist.get(i).getTemperature().getTemp();
            int temps = (int) (temp -273.15);
            temperature.setText(String.valueOf(temps));
        }


        int st;
        String image = forecastlist.get(i).getWeather().get(0).getIcon();
        image = "@drawable/"+"_"+image;
        int imageResource = context.getResources().getIdentifier(image, null, context.getPackageName());
        Drawable res = context.getResources().getDrawable(imageResource);
        forecastIcon.setImageDrawable(res);


        date = forecastlist.get(i).getDateTime();
        //Calendar calendar = Calendar.getInstance();
       // calendar.setTimeInMillis(date);
        //Log.d("Time", calendar.getTime().toString());

        Calendar mydate = Calendar.getInstance();
        mydate.setTimeInMillis(date*1000);
        forecastDateDay.setText(String.valueOf(mydate.get(Calendar.DAY_OF_MONTH)));
        forecastDateHour.setText(String.valueOf(mydate.get(Calendar.HOUR_OF_DAY))+"h");
        //Log.d("DATE",mydate.get(Calendar.DAY_OF_MONTH)+"."+mydate.get(Calendar.MONTH)+"."+mydate.get(Calendar.YEAR));

       /* long timestamp = date;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        int vv= cal.get(Calendar.DAY_OF_MONTH);
        Log.d("Time", String.valueOf(vv));
*/

        return fview;
    }
}
