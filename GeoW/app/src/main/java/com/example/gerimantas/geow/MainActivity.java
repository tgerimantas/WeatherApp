package com.example.gerimantas.geow;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity  implements ActionBar.TabListener  {

     String SelectedCityWeatherInfo;
    String SelectedCityForecastInfo;
     String TemperatureRate;
    private FragmentPagerAdapter fragmentPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.selected_weather_actionbar, null);
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        SharedPreferences sharedPreferences = getSharedPreferences("default", MODE_PRIVATE);
        SelectedCityWeatherInfo = sharedPreferences.getString("SelectedWeather", "");
        SelectedCityForecastInfo = sharedPreferences.getString("SelectedForecast", "");
        TemperatureRate = sharedPreferences.getString("TemperatureRate", "");


        final int PAGE_COUNT = 2;
        final String tabtitles[] = new String[] { "Today", "Forecast" };

        fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public int getCount() {
                return PAGE_COUNT;
            }

            @Override
            public Fragment getItem(int position) {
                switch (position) {

                    case 0:

                            Bundle today = new Bundle();
                            today.putString("InfoWeather", SelectedCityWeatherInfo);
                            today.putString("TemperatureRate", TemperatureRate);
                           // Log.d("AAA",bundle.getString("InfoWeather"));
                            FragmentTodayWeather fragmentTodayWeather = new FragmentTodayWeather();
                            fragmentTodayWeather.setArguments(today);

                        return fragmentTodayWeather;

                    case 1:
                        Bundle forecast = new Bundle();
                        forecast.putString("Forecast", SelectedCityForecastInfo);
                        forecast.putString("TemperatureRate", TemperatureRate);
                        FragmentForecast fragmentForecast = new FragmentForecast();
                        fragmentForecast.setArguments(forecast);

                        return fragmentForecast;

                }
                return null;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return tabtitles[position];
            }

        };

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(fragmentPagerAdapter);


    }



    public void BackToMain(View view) {
        Toast.makeText(MainActivity.this, "Back Clicked", Toast.LENGTH_SHORT).show();
        super.onResume();
        finish();
    }

    public void Refresh(View view) {
        Toast.makeText(MainActivity.this, "Refresh Clicked", Toast.LENGTH_SHORT).show();
       // WeatherTask task = new WeatherTask();
        // task.execute(newPlace+TemperatureRate);
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction fragmentTransaction) {

    }
}
